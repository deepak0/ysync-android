YouTube-Sync (Ysync ) is an open source project for downloading audio files (mp3 versions) of YouTube videos directly to your android phone.
We offer both android app and chrome extension to download audio versions of YouTube videos.

This product is developed by two enhusiastic programmers Deepak Sharma and Pranav Tiwari.

PS: For getting app and chrome extension contact the developers at either of the two mail-ids: 
    deepak.sharma3343@gmail.com,
    pranav15197@gmail.com