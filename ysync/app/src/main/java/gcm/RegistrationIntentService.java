package gcm;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.sync.youtube.ysync.R;

import model.QuickPreferences;
import model.SharedPreferenceManager;

/**
 * Created by deepak on 19/2/16.
 */
public class RegistrationIntentService extends IntentService {
    private static final String TAG = "RegIntentService";
    private SharedPreferenceManager sharedPreferences;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = SharedPreferenceManager.getInstance(this);
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            sendRegistrationToServer(token);
            sharedPreferences.putBoolean(QuickPreferences.SENT_TOKEN_TO_SERVER, true);
        } catch (Exception e){
            Log.d(TAG, "Failed to complete token refresh", e);
            sharedPreferences.putBoolean(QuickPreferences.SENT_TOKEN_TO_SERVER, false);
        }
        Intent registrationComplete = new Intent(QuickPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String token){
        Log.d(TAG, "GCM Registration Token: " + token);
        sharedPreferences.putString(QuickPreferences.REG_ID, token);
    }
}
