package gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.sync.youtube.ysync.MainActivity;
import com.sync.youtube.ysync.R;

import Helper.DownloadManagerHelper;

/**
 * Created by deepak on 19/2/16.
 */

public class MyGcmListenerService extends GcmListenerService{
    private static final String TAG = "MyGcmListenerService";

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        String notificationId = data.getString("notificationId");
        String title;
        try {
            title = data.getString("title");
        } catch (Exception e){
            Log.d(TAG, "Title not found:", e);
            title = "audio";
        }
        Log.d(TAG, "gcm From: " + from);
        Log.d(TAG, "gcm Message: " + message);

        //sendNotification(message, Integer.valueOf(notificationId), title);
        downloadAudio(message, title);
    }

    private void sendNotification(String message, Integer notificationId, String title) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notif48)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private void downloadAudio(String url, String title){
        DownloadManagerHelper downloadManagerHelper = new DownloadManagerHelper();
        downloadManagerHelper.DownloadVideo(getApplicationContext(), url);
    }
}