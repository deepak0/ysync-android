package Helper;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.LoginFilter;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

import model.QuickPreferences;

/**
 * Created by pranav on 07/03/16.
 */
public class DownloadManagerHelper{
    private String TAG = "Download Manager Helper";
    private Activity mActivity;
    private Context mContext;
    private String url;
    public Boolean mPermissionGranted = false;

    public void DownloadVideo(Activity activity, String url){
        mActivity = activity;
        mContext = activity;
        this.url = url;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) permissionCheck();
        else mPermissionGranted = true;
        initializeDownload();
    }

    public void DownloadVideo(Context context, String url){
        mContext = context;
        this.url = url;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) permissionCheck();
        else mPermissionGranted = true;
        initializeDownload();
    }

    private void permissionCheck(){
        int permissionCheck = ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Log.d(TAG, "User Requests Permission Explanation");
            }
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    QuickPreferences.PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        } else mPermissionGranted = true;
    }

    public void initializeDownload(){
        if (mPermissionGranted){
            String serviceString = Context.DOWNLOAD_SERVICE;
            DownloadManager downloadManager;
            downloadManager = (DownloadManager)mContext.getSystemService(serviceString);

            Uri uri = Uri.parse(cleanUrl(url));
            DownloadManager.Request request = new DownloadManager.Request(uri);

            File mFolder = new File(Environment.getExternalStorageDirectory(), "ySync");

            if (!mFolder.exists()) {
                mFolder.mkdirs();
                mFolder.setExecutable(true);
                mFolder.setReadable(true);
                mFolder.setWritable(true);
            }

            request.setDestinationInExternalPublicDir("/ySync/", uri.getLastPathSegment());
            request.allowScanningByMediaScanner();


            long reference = downloadManager.enqueue(request);
        }
    }


    public void RegisterDownloadManagerReciever(Context context) {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    DownloadCompleted();
                }
            }
        };

        context.registerReceiver(receiver, new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    private void DownloadCompleted(){
        Log.d(TAG, "Download Finished");


    }

    private String cleanUrl(String url){
        url = url.replaceAll(" ", "%20");
        return url;
    }

}
