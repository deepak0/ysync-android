package model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by deepak on 29/2/16.
 */
public class SharedPreferenceManager {
    private Context mContext;
    private SharedPreferences mSharedPreferences;
    private static String PREFIX;

    public SharedPreferenceManager(Context context) {
        mContext = context;
        PREFIX = mContext.getPackageName();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public static SharedPreferenceManager getInstance(Context context){
        return new SharedPreferenceManager(context);
    }

    public void putString(String name, String value){
        String key = PREFIX+name;
        mSharedPreferences.edit()
                .putString(key, value)
                .apply();
    }

    public String getString(String name, String default_value){
        String key = PREFIX+name;
        return mSharedPreferences.getString(key, default_value);
    }

    public String getString(String name){
        return getString(name, null);
    }

    public void putBoolean(String name, Boolean value){
        String key = PREFIX+name;
        mSharedPreferences.edit()
                .putBoolean(key, value)
                .apply();
    }

    public Boolean getBoolean(String name, Boolean default_value){
        String key = PREFIX+name;
        return mSharedPreferences.getBoolean(key, default_value);
    }

    public Boolean getBoolean(String name){
        return getBoolean(name, false);
    }
}
