package model;

/**
 * Created by deepak on 29/2/16.
 */
public class QuickPreferences {
    public static final String ACCESS_CODE = "access_code";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String REG_ID = "regId";
    public static final String LOGGED_IN = "login";
    public static final String BASE_URL = "http://54.200.99.116/";

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String DEVICE_TYPE = "1";

    public static final int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 0;
}
