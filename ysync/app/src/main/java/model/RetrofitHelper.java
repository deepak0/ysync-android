package model;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import api.YsyncAPI;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by deepak on 5/3/16.
 */
public class RetrofitHelper {
    private static final String TAG = "RetrofitHelper";
    private static Context mContext;
    private static YsyncAPI mAPI;

    private static YsyncAPI RetrofitObject(Context context) {
        mContext = context;

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(QuickPreferences.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mAPI = retrofit.create(YsyncAPI.class);
        return mAPI;
    }

    public static YsyncAPI getInstance(Context context){
        return RetrofitObject(context);
    }
}
