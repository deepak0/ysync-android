package api;

import model.AudioURL;
import model.User;
import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by deepak on 19/2/16.
 */
public interface YsyncAPI {
    @GET("/ySync/get_url_for_mobile/")
    Call<AudioURL> getDownloadUrl(@Query("url") String url,
                                  @Query("code") String code);

    @FormUrlEncoded
    @POST("/ySync/login/")
    Call<User> createUser(@Field("email") String email,
                          @Field("device_id") String device_id,
                          @Field("device_type") String device_type);
}
