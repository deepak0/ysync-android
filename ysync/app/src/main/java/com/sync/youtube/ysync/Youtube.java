package com.sync.youtube.ysync;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Helper.DownloadManagerHelper;
import api.YsyncAPI;
import model.AudioURL;
import model.QuickPreferences;
import model.RetrofitHelper;
import model.SharedPreferenceManager;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by deepak on 4/3/16.
 */
public class Youtube extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{
    private static final String TAG = "Youtube";
    private static final String YouTube_URL = "https://www.youtube.com/";
    private static final String YouTube_Video_REGEX = ".*(v=).*";
    private Pattern mPattern = Pattern.compile(YouTube_Video_REGEX);
    private WebView mWebView;
    private YsyncAPI mAPI;
    private String Web_URL = YouTube_URL;
    private List<String> mAllowedURL = new ArrayList<>();
    private DownloadManagerHelper downloadManagerHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent.getData()!=null){
            Uri data = intent.getData();
            String scheme = data.getScheme();
            String specificPart = data.getEncodedSchemeSpecificPart();
            Web_URL = scheme + "://" + specificPart;
        }

        declareAllowedURL();

        mWebView = new WebView(this);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);

        CookieManager.getInstance().setAcceptCookie(true);

        mWebView.setWebViewClient(mWebViewClient);
        mWebView.loadUrl(Web_URL);
        setContentView(mWebView);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) forceOverflowMenu();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private WebViewClient mWebViewClient = new WebViewClient(){
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Snackbar.make(mWebView.findFocus(), "Error Loading Youtube...", Snackbar.LENGTH_LONG).show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            mWebView.setWebChromeClient(mWebChromeClient);
            String domain = Uri.parse(url).getHost();
            Log.d(TAG, "Current URL:"+url);
            if (mAllowedURL.contains(domain)){
                mWebView.loadUrl(url);
                return true;
            } else return true;
        }
    };

    private WebChromeClient mWebChromeClient = new WebChromeClient() {
        private View mCustomView;
        @Override
        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
        }
    };

    public String getWebViewURL(){
        return mWebView.getUrl();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String code = SharedPreferenceManager.getInstance(this)
                .getString(QuickPreferences.ACCESS_CODE);
        switch (id){
            case R.id.action_download:
                String url = getWebViewURL();
                Matcher matcher = mPattern.matcher(url);
                if (matcher.matches()){

                    Log.d(TAG, "Youtube download --> " + url);
                    Snackbar.make(mWebView.findFocus(),
                            "Sending Download Request ...", Snackbar.LENGTH_LONG).show();
                    mAPI = RetrofitHelper.getInstance(this);
                    mAPI.getDownloadUrl(url, code)
                            .enqueue(mAudioURLCallback);
                    return true;
                } else {
                    Snackbar.make(mWebView.findFocus(), "Not a YouTube Video!", Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.access_code:
                Snackbar.make(mWebView.findFocus(), "Access Code: "+code, Snackbar.LENGTH_LONG).show();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "Permissions: Activity Callback Received");
        switch (requestCode) {
            case QuickPreferences.PERMISSIONS_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Permission Granted");
                    if (downloadManagerHelper != null){
                        downloadManagerHelper.mPermissionGranted = true;
                        downloadManagerHelper.initializeDownload();
                    }
                } else {
                    Log.d(TAG, "Permission Denied");
                }
                break;
            }
        }
    }

    private Callback<AudioURL> mAudioURLCallback = new Callback<AudioURL>() {
        @Override
        public void onResponse(Response<AudioURL> response, Retrofit retrofit) {
            AudioURL audioURL = response.body();
            Log.d(TAG, "Youtube Retrofit Title: " + audioURL.getTitle());
            Log.d(TAG, "Youtube Retrofit Result: " + audioURL.getResult());
            Log.d(TAG, "Youtube Retrofit Url: " + audioURL.getUrl());
            downloadAudio(audioURL.getUrl(), audioURL.getTitle());
        }

        @Override
        public void onFailure(Throwable t) {
            Log.d(TAG, "Retrofit:Error", t);
        }
    };

    private void downloadAudio(String url, String title){
        Snackbar.make(mWebView.findFocus(),
                "Downloading ...", Snackbar.LENGTH_LONG).show();

        Log.d(TAG,url);
        downloadManagerHelper = new DownloadManagerHelper();
        downloadManagerHelper.DownloadVideo(Youtube.this, url);
    }

    private void forceOverflowMenu(){
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        }
        catch (Exception e) {
            Log.d(TAG, "Error showing Overflow Menu", e);
        }
    }

    private void declareAllowedURL(){
        mAllowedURL.add("www.youtube.com");
        mAllowedURL.add("youtube.com");
        mAllowedURL.add("m.youtube.com");
        mAllowedURL.add("accounts.youtube.com");
        mAllowedURL.add("accounts.google.com");
        mAllowedURL.add("accounts.google.co.in");
        mAllowedURL.add("www.google.co.in");
        mAllowedURL.add("www.google.com");
    }
}
