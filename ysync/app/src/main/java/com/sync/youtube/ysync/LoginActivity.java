package com.sync.youtube.ysync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import api.YsyncAPI;
import gcm.RegistrationIntentService;
import model.QuickPreferences;
import model.RetrofitHelper;
import model.SharedPreferenceManager;
import model.User;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by deepak on 19/2/16.
 */
public class LoginActivity extends AppCompatActivity{
    private static final String TAG = "LoginActivity";
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private SharedPreferenceManager sharedPreferences;
    private EditText mEmailText;
    private Button mLoginButton;
    private YsyncAPI mAPI;
    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = SharedPreferenceManager.getInstance(this);

        if (sharedPreferences.getBoolean(QuickPreferences.LOGGED_IN, false)){
            Intent intent = new Intent(LoginActivity.this, Youtube.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        setContentView(R.layout.login_activity);

        mContext = this;

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickPreferences.SENT_TOKEN_TO_SERVER);
                if (sentToken) {
                    Log.d(TAG, "Token sent to server");
                } else {
                    Log.d(TAG, "Error in fetching InstanceID");
                }
            }
        };

        if (checkPlayServices()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        mEmailText = (EditText)findViewById(R.id.login_box);
        mLoginButton = (Button)findViewById(R.id.login_button);

        mLoginButton.setOnClickListener(mLoginListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode,
                        QuickPreferences.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private View.OnClickListener mLoginListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String email = mEmailText.getText().toString();
            if (email.length()>0){
                String device_id = sharedPreferences.getString(QuickPreferences.REG_ID);

                mAPI = RetrofitHelper.getInstance(mContext);
                Call<User> call = mAPI.createUser(email, device_id, QuickPreferences.DEVICE_TYPE);
                call.enqueue(mUserCallback);
            }
        }
    };

    private Callback<User> mUserCallback = new Callback<User>() {
        @Override
        public void onResponse(Response<User> response, Retrofit retrofit) {
            User user = response.body();
            Log.d(TAG, "Retrofit User: Code=" + user.getCode()
                    + "Message=" + user.getMessage()
                    + "Result=" + user.getResult());
            sharedPreferences.putString(QuickPreferences.ACCESS_CODE, user.getCode());
            sharedPreferences.putBoolean(QuickPreferences.LOGGED_IN, true);
            Intent intent = new Intent(LoginActivity.this, Youtube.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        @Override
        public void onFailure(Throwable t) {
            Log.d(TAG, "Retrofit:Error", t);
        }
    };
}
