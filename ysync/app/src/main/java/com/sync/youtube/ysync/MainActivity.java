package com.sync.youtube.ysync;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import Helper.DownloadManagerHelper;
import model.QuickPreferences;
import model.SharedPreferenceManager;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private SharedPreferenceManager mSharedPreferences;
    private DownloadManagerHelper mDownloadManagerHelper;
    private TextView mAccessCode;
    private Button mLogout;
    private Button mLaunchYoutube;
    private Button mDownloadManagerTest;
    private Boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSharedPreferences = SharedPreferenceManager.getInstance(this);
        String access_code = mSharedPreferences.getString(QuickPreferences.ACCESS_CODE);
        mAccessCode = (TextView)findViewById(R.id.access_code);
        mAccessCode.setText(access_code);

        mLogout = (Button)findViewById(R.id.logout);
        mLogout.setOnClickListener(mLogoutListener);

        mLaunchYoutube = (Button)findViewById(R.id.launch_youtube);
        mLaunchYoutube.setOnClickListener(mLaunchYoutubeListener);
;

        mDownloadManagerHelper = new DownloadManagerHelper();
        mDownloadManagerHelper.RegisterDownloadManagerReciever(this);
    }


    private View.OnClickListener mLogoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mSharedPreferences.putBoolean(QuickPreferences.LOGGED_IN, false);
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    };

    private View.OnClickListener mLaunchYoutubeListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this, Youtube.class);
            startActivity(intent);
        }
    };

    @Override
    public void onBackPressed() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }
}
